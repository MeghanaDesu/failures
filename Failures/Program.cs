﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;

namespace Failures
{
    public class Program
    {
        public static void Main(string[] args)
        {
			var oldPendingTasks = 1;
			var oldTasks = 1;
			var date = 1;
			var connectionString = ConfigurationManager.ConnectionStrings["DeltaXCoreContext"].ConnectionString;
            var failures = new List<Failure>();
			var taskTypes = "268,273,274,277,280,283,287,289,290,298,299,309,312";
			var tablename = oldPendingTasks == 1 ? "pendingtasksarchive" : "pendingtasks";
			var taskstablename = oldTasks == 1 ? "tasksarchive" : "tasks";
			var dictionary = new Dictionary<int, Result>();
			using (var connection = new SqlConnection(connectionString))
				failures = connection.Query<Failure>(
					$@"SELECT [PT].[Id]
						,[PT].[TaskTypeId]
						,SUBSTRING([TL].[Description],1,100) [Msg]
                        ,[PT].[CurrentRetryCount]
                        ,[T].[MaxRetryCount]
						,[PT].[Taskstarttime]
						,[PT].[Agencyid]
					FROM [{tablename}] pt WITH (NOLOCK)
					JOIN [{taskstablename}] [T] WITH (NOLOCK) ON [pt].[Taskid] = [t].[id] 
					JOIN TaskLogs [TL] WITH (NOLOCK) ON pt.id = [TL].[pendingTaskid]
					WHERE [PT].[TaskTypeId] IN ({taskTypes})
						AND pt.tasktrigertime > convert(DATE, getdate() - {date})
						AND pt.tasktrigertime < convert(DATE, getdate() + 1 - {date})
						AND pt.TaskStatusId = 10",
					commandTimeout: 500).ToList();

			foreach (var failure in failures)
			{
				var isCompleteFailure = failure.CurrentRetryCount == failure.MaxRetryCount;
				if (dictionary.ContainsKey(failure.TaskTypeId))
                {
					var result = dictionary[failure.TaskTypeId];
					result.Failures++;
					if (isCompleteFailure) result.CompleteFailures++;
					result.Msgs.Add($"   Id:{failure.Id}, {failure.Msg} ({failure.AgencyId}) ({failure.TaskStartTime})");
                }
                else
				{
					dictionary.Add(failure.TaskTypeId, new Result
					{
						Failures = 1,
						Msgs = new List<string>() { $"   Id:{failure.Id}, {failure.Msg} ({failure.AgencyId}) ({failure.TaskStartTime})" },
						CompleteFailures = isCompleteFailure ? 1 : 0
					});
				}
			}
			var isLag = false;
			var exeMaxLag = new List<dynamic>();
			using (var connection = new SqlConnection(connectionString))
				exeMaxLag = connection.Query<dynamic>(sql:
					$@"SELECT MAX(DATEDIFF(minute, tasktrigertime, TaskEndTime)) AS MaxExe
						,TaskTypeId
					FROM [{tablename}] [PT] WITH(NOLOCK)
					WHERE tasktypeid IN ({taskTypes})
						AND DATEDIFF(minute, tasktrigertime, TaskEndTime) >= 30
						AND tasktrigertime > convert(DATE, getdate() - {date})
						AND tasktrigertime < convert(DATE, getdate()  + 1 - {date})
					GROUP BY [PT].[TaskTypeId]", commandTimeout: 500).ToList();

			foreach (var l in exeMaxLag)
			{
				if (dictionary.ContainsKey(l.TaskTypeId))
				{
					var result = dictionary[l.TaskTypeId];
					result.MaxExe = l.MaxExe;
				}
				else
				{
					dictionary.Add(l.TaskTypeId, new Result
					{
						MaxExe = l.MaxExe
					});
				}
				isLag = true;
			}

			var waitMaxLag = new List<dynamic>();
			using (var connection = new SqlConnection(connectionString))
				waitMaxLag = connection.Query<dynamic>(sql:
					$@"SELECT MAX(DATEDIFF(minute, TaskStartTime, tasktrigertime)) AS MaxWait
						,TaskTypeId
					FROM [{tablename}] [PT] WITH(NOLOCK)
					WHERE tasktypeid IN ({taskTypes})
						AND DATEDIFF(minute, TaskStartTime, tasktrigertime) >= 30
						AND tasktrigertime > convert(DATE, getdate() - {date})
						AND tasktrigertime < convert(DATE, getdate()  + 1 - {date})
						AND createdat < taskstarttime
					GROUP BY [PT].[TaskTypeId]", commandTimeout: 500).ToList();

			foreach (var l in waitMaxLag)
			{
				if (dictionary.ContainsKey(l.TaskTypeId))
				{
					var result = dictionary[l.TaskTypeId];
					result.MaxWait = l.MaxWait;
				}
				else
				{
					dictionary.Add(l.TaskTypeId, new Result
					{
						MaxWait = l.MaxWait
					});
				}
				isLag = true;
			}

			var waitAvgLag = new List<dynamic>();
			using (var connection = new SqlConnection(connectionString))
				waitAvgLag = connection.Query<dynamic>(sql:
				   $@"SELECT AVG(DATEDIFF(minute, TaskStartTime, tasktrigertime)) AS AvgWait
						,TaskTypeId
					FROM [{tablename}] [PT] WITH(NOLOCK)
					WHERE tasktypeid IN ({taskTypes})
						AND tasktrigertime > convert(DATE, getdate() - {date})
						AND tasktrigertime < convert(DATE, getdate()  + 1 - {date})
						AND createdat < taskstarttime
					GROUP BY [PT].[TaskTypeId] HAVING AVG(DATEDIFF(minute, TaskStartTime, tasktrigertime))>=30", commandTimeout: 500).ToList();

			foreach (var l in waitAvgLag)
			{
				if (dictionary.ContainsKey(l.TaskTypeId))
				{
					var result = dictionary[l.TaskTypeId];
					result.AvgWait = l.AvgWait;
				}
				else
				{
					dictionary.Add(l.TaskTypeId, new Result
					{
						AvgWait = l.AvgWait
					});
				}
				isLag = true;
			}

			var exeAvgLag = new List<dynamic>();
			using (var connection = new SqlConnection(connectionString))
				exeAvgLag = connection.Query<dynamic>(sql:
				   $@"SELECT AVG(DATEDIFF(minute, tasktrigertime, TaskEndTime)) AS AvgExe
						,TaskTypeId
					FROM [{tablename}] [PT] WITH(NOLOCK)
					WHERE tasktypeid IN ({taskTypes})
						AND tasktrigertime > convert(DATE, getdate() - {date})
						AND tasktrigertime < convert(DATE, getdate()  + 1 - {date})
					GROUP BY [PT].[TaskTypeId] HAVING AVG(DATEDIFF(minute, tasktrigertime, TaskEndTime))>=30", commandTimeout: 500).ToList();

			foreach (var l in exeAvgLag)
			{
				if (dictionary.ContainsKey(l.TaskTypeId))
				{
					var result = dictionary[l.TaskTypeId];
					result.AvgExe = l.AvgExe;
				}
				else
				{
					dictionary.Add(l.TaskTypeId, new Result
					{
						AvgExe = l.AvgExe
					});
				}
				isLag = true;
			}

			foreach (var t in dictionary)
            {
				using (var connection = new SqlConnection(connectionString))
					t.Value.Name = connection.QuerySingle<string>(sql:$"SELECT TITLE from tasktypes with(nolock) where id={t.Key}", commandTimeout: 500);
				Console.WriteLine($"{t.Value.Name}({t.Key});");
				if(t.Value.Failures > 0) Console.WriteLine($"Failures-{t.Value.Failures}{ (t.Value.CompleteFailures > 0 ? $"({t.Value.CompleteFailures} Complete)" : string.Empty) }");
				foreach (var msg in t.Value.Msgs) Console.WriteLine(msg);
				if(t.Value.AvgExe >= 30) Console.WriteLine("AvgExe: " + t.Value.AvgExe);
				if(t.Value.AvgWait >= 30) Console.WriteLine("AvgWait: " + t.Value.AvgWait);
				if(t.Value.MaxExe >= 30) Console.WriteLine("MaxExe: " + t.Value.MaxExe);
				if (t.Value.MaxWait >= 30) Console.WriteLine("MaxWait: " + t.Value.MaxWait);
				Console.WriteLine();
			}

			if (isLag) Console.WriteLine($@"Get lagging pendingtask details
select * from pendingtasks with(nolock) where tasktypeid=##
--AND
--DATEDIFF(minute, TaskStartTime, tasktrigertime) >= 30
--DATEDIFF(minute, tasktrigertime, TaskEndTime) >= 30
						AND tasktrigertime > convert(DATE, getdate() - {date})
						AND tasktrigertime < convert(DATE, getdate() + 1 - {date})
						AND createdat < taskstarttime");

		}
    }
}
