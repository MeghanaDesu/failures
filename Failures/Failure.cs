﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Failures
{
    public class Failure
    {
        public int TaskTypeId { get; set; }
        public long Id { get; set; }
        public string Msg { get; set; }
        public long CurrentRetryCount { get; set; }
        public long MaxRetryCount { get; set; }
        public long AgencyId { get; set; }
        public DateTime TaskStartTime { get; set; }
    }
}
