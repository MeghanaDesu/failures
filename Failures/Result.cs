﻿using System.Collections.Generic;

namespace Failures
{
    public class Result
    {
        public string Name { get; set; }
        public long Failures { get; set; }
        public long CompleteFailures { get; set; }
        public List<string> Msgs { get; set; } = new List<string>();
        public long MaxExe { get; set; }
        public long MaxWait { get; set; }
        public long AvgExe { get; set; }
        public long AvgWait { get; set; }
    }
}
